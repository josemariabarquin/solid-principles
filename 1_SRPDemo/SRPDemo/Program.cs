﻿
namespace ConsoleUI
{
    // *** S O L I D  Principles ***
    // https://www.youtube.com/watch?v=5RwhyZnVRS8&list=PLAaFb7UfyShCoS246UzZJNEiXuD8bg02e
    // 1.- Single Responsibility Principle (SRP)
    // 
    // Establece que cada módulo o clase debe tener responsabilidad sobre una sola parte de la funcionalidad 
    // proporcionada por el software y esta responsabilidad debe estar encapsulada en su totalidad por la clase. 
    // Todos sus servicios deben estar estrechamente alineados con esa responsabilidad.
    // 

    class Program
    {
        static void Main(string[] args)
        {
            StandardMessages.WelcomeMessage();
            Person user = PersonDataCapture.Capture();

            bool isUserValid = PersonValidator.Validate(user);

            if (isUserValid == false)
            {
                StandardMessages.EndApplication();
                return;
            }

            AccountGenerator.CreateAccount(user);
            StandardMessages.EndApplication();
        }
    }
}
