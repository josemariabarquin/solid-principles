﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoLibrary;

namespace ConsoleUI
{
    class Program
    {
        /// <summary>
        /// 4.- Interface Segregation Principle
        /// It could be defined as: “Clients should not be forced to depend upon interfaces that they do not use.”
        /// 
        /// The Interface Segregation Principle (ISP) states that a client should not be exposed to methods it doesn't need. 
        /// Declaring methods in an interface that the client doesn't need pollutes the interface and leads to a 
        /// “bulky” or “fat” interface.
        /// 
        /// https://www.youtube.com/watch?v=y1JiMGP51NE
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            IBorrowableDVD dvd = new DVD();
            
            Console.ReadLine();
        }
    }
}
