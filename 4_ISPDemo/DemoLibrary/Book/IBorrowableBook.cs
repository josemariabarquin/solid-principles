﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLibrary
{
    // Empty interface
    public interface IBorrowableBook : IBorrowable, IBook
    {
    }
}
