﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OCPLibrary;

namespace ConsoleUI
{
    class Program
    {
        /// <summary>
        /// https://www.youtube.com/watch?v=VFlk43QGEgc
        /// 
        /// OPEN / CLOSED PRINCIPLE (OCP)
        /// 
        ///  The open–closed principle states: 
        ///  "software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification"; 
        ///  that is, such an entity can allow its behaviour to be extended without modifying its source code.
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            List<IApplicantModel> applicants = new List<IApplicantModel>
            {
                new PersonModel { FirstName = "Tim", LastName = "Corey" },
                new ManagerModel { FirstName = "Sue", LastName = "Storm" },
                new ExecutiveModel { FirstName = "Nancy", LastName = "Roman" },
                new TechnicianModel { FirstName = "Robert", LastName = "Green" }
            };

            List<EmployeeModel> employees = new List<EmployeeModel>();
            Accounts accountProcessor = new Accounts();

            foreach (var person in applicants)
            {
                employees.Add(person.AccountProcessor.Create(person));
            }

            foreach (var emp in employees)
            {
                Console.WriteLine($"{ emp.FirstName } { emp.LastName } : { emp.EmailAddress } Ismanager: {emp.IsManager} IsExecutive: {emp.IsExecutive}" );
            }

            Console.ReadLine();
        }
    }
}
