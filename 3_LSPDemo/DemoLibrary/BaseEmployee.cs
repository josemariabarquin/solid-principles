﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLibrary
{
    /// <summary>
    /// Data abstraction is the process of hiding certain details and showing only essential information to the user.
    /// Abstraction can be achieved with either abstract classes or interfaces.
    ///
    /// The abstract keyword is used for classes and methods:
    /// .- Abstract class: is a restricted class that cannot be used to create objects(to access it, it must be inherited from another class).
    /// .- Abstract method: can only be used in an abstract class, and it does not have a body.The body is provided by the derived class (inherited from).
    /// </summary>
    public abstract class BaseEmployee : IEmployee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Salary { get; set; }

        /// <summary>
        /// C# virtual method is a method that can be redefined in derived classes. 
        /// In C#, a virtual method has an implementation in a base class as well as derived the class. 
        /// It is used when a method's basic functionality is the same but sometimes more functionality is needed in the derived class. 
        /// A virtual method is created in the base class that can be overriden in the derived class. 
        /// We create a virtual method in the base class using the virtual keyword and that method is overriden in the derived class 
        /// using the override keyword.
        /// </summary>
        /// <param name="rank"></param>
        public virtual void CalculatePerHourRate(int rank)
        {
            decimal baseAmount = 12.50M;
            Salary = baseAmount + (rank * 2);
        }
    }
}
