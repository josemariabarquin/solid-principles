﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoLibrary;

namespace ConsoleUI
{
    class Program
    {
        /// <summary>
        /// https://www.youtube.com/watch?v=-3UXq2krhyw
        /// https://www.alpharithms.com/liskov-substitution-principle-lsp-solid-114908/
        /// 3.- The Liskov Substitution Principle (LSP) is one of five SOLID object-oriented design principles. 
        /// It states that a superclass object should be replaceable with a subclass object without breaking 
        /// the functionality of the software. It is a type of behavioral subtyping defined by semantic, 
        /// rather than syntactic, design consideration.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            IManager accountingVP = new Manager();
            accountingVP.FirstName = "Emma";
            accountingVP.LastName = "Stone";
            accountingVP.CalculatePerHourRate(4);

            IManaged emp = new Employee();

            emp.FirstName = "Tim";
            emp.LastName = "Corey";
            emp.AssignManager(accountingVP);
            emp.CalculatePerHourRate(2);

            Console.WriteLine($"{ emp.FirstName }'s salary is ${ emp.Salary }/hour.");

            Console.ReadLine();
        }
    }
}
