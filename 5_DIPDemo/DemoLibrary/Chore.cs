﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLibrary
{
    public class Chore : IChore
    {
        // WITHOUT DEPENDENCY INVERSION: no se usa el interface

        // Chore depend on Looger and Emailer classes

        //public string ChoreName { get; set; }
        //public Person Owner { get; set; }
        //public double HoursWorked { get; private set; }
        //public bool IsComplete { get; private set; }

        //public void PerformedWork(double hours)
        //{
        //    HoursWorked += hours;
        //    Logger log = new Logger();
        //    log.Log($"Performed work on {ChoreName}");
        //}

        //public void CompleteChore()
        //{
        //    IsComplete = true;

        //    Logger log = new Logger();
        //    log.Log($"Completed {ChoreName}");

        //    Emailer emailer = new Emailer();
        //    emailer.SendMessage(Owner, $"The chore {ChoreName} is complete.");
        //}

        // CON DEPENDENCY INVERSION
        ILogger _logger;
        IMessageSender _messageSender;

        public string ChoreName { get; set; }
        public IPerson Owner { get; set; }
        public double HoursWorked { get; private set; }
        public bool IsComplete { get; private set; }

        public Chore(ILogger logger, IMessageSender messageSender)
        {
            _logger = logger;
            _messageSender = messageSender;
        }

        public void PerformedWork(double hours)
        {
            HoursWorked += hours;
            _logger.Log($"Performed work on { ChoreName }");
        }

        public void CompleteChore()
        {
            IsComplete = true;

            _logger.Log($"Completed { ChoreName }");

            _messageSender.SendMessage(Owner, $"The chore { ChoreName } is complete.");
        }
    }
}
