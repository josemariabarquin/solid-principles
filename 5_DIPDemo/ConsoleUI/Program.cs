﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoLibrary;

namespace ConsoleUI
{
    /// <summary>
    /// 5.- Dependency Inversion Principle
    /// https://www.youtube.com/watch?v=y1JiMGP51NE
    /// 
    /// In object-oriented design, the dependency inversion principle is a specific form of loosely coupling software modules. 
    /// When following this principle, the conventional dependency relationships established from high-level, policy-setting modules 
    /// to low-level, dependency modules are reversed, thus rendering high-level modules independent of the low-level 
    /// module implementation details. The principle states:
    /// [A] High-level modules should not depend on low-level modules.Both should depend on abstractions (e.g., interfaces).
    /// [B] Abstractions should not depend on details.Details(concrete implementations) should depend on abstractions.
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            /* WITHOUT DEPENDENCY INVERSION
                MODULE PROGRAM depend on Person and Chore class
                We see the high level modules 'chore' and 'program' depend on low level ones meaning:
                Couldn't just change out how to they say logger works because that would break chore which and break our program
                and both should depend on abstractions, and those abstractions should not depend on details
                we can do this using interfaces.
                Those abstractions should not have to know about how things get done, just say these are the things that will get done.
            */

            //Person owner_ = new Person
            //{
            //    FirstName = "Tim",
            //    LastName = "Corey",
            //    EmailAddress = "tim@Iamtimcorey.com",
            //    PhoneNumber = "555-1212"
            //};

            //Chore chore_ = new Chore
            //{
            //    ChoreName = "Take out the trash",
            //    Owner = owner_
            //};

            //chore_.PerformedWork(3);
            //chore_.PerformedWork(1.5);
            //chore_.CompleteChore();

            //Console.ReadLine();



            // CON DEPENDENCY INVERSION, I break Person and Chore con interfaces
            IPerson owner = Factory.CreatePerson(); // Here I create the inversion of dependency for person

            owner.FirstName = "Tim";
            owner.LastName = "Corey";
            owner.EmailAddress = "tim@Iamtimcorey.com";
            owner.PhoneNumber = "555-1212";

            IChore chore = Factory.CreateChore(); // Here I create the inversion of dependency for chore
            chore.ChoreName = "Take out the trash";
            chore.Owner = owner;
      
            chore.PerformedWork(3);
            chore.PerformedWork(1.5);
            chore.CompleteChore();

            Console.ReadLine();
        }
    }
}
